cordova.define("cordova-plugin-twilio-video-ios.TwilioVideoIOS", function(require, exports, module) {
var exec = require('cordova/exec');

exports.openRoomForGuest = function(token, room, avatarUrl, options, success, error) {
    exec(success, error, "TwilioVideoIOS", "openRoomForGuest", [token, room, avatarUrl, options]);
};

exports.openRoomForHost = function(token, room, options, success, error) {
    exec(success, error, "TwilioVideoIOS", "openRoomForHost", [token, room, options]);
};

exports.closeRoom = function(success, error) {
    exec(success, error, "TwilioVideoIOS", "closeRoom", []);
};

exports.waitStatus = function(success, error) {
    exec(success, error, "TwilioVideoIOS", "waitStatus", []);
};

});
