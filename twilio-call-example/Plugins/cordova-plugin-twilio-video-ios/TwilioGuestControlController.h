//
//  TwilioGuestControlController.h
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import <UIKit/UIKit.h>
#import "TwilioCientControl.h"

@interface TwilioGuestControlController : UIViewController<TwilioCientControl>

@end
