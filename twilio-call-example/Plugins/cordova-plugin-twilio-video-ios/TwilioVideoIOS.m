/********* TwilioVideoIOS.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import "TwilioClientController.h"
#import "TwilioPluginDelegate.h"

/*
 Available statuses
 
     CONNECTED_TO_ROOM
     PARTICIPANT_CONNECTED
     PARTICIPANT_DISCONNECTED
     CONNECTION_LOST
     HANGED_UP
     VERIFICATION_PASSED
     VERIFICATION_DECLINED
*/

@interface TwilioVideoIOS : CDVPlugin <TwilioPluginDelegate> {
    
}

@property (strong, nonatomic) UINavigationController *containerController;
@property (strong, nonatomic) TwilioClientController *twilioController;
@property (strong, nonatomic) CDVInvokedUrlCommand* waitStatusCommand;

@end

@implementation TwilioVideoIOS

- (void)openRoomForGuest:(CDVInvokedUrlCommand*)command {
    
    if (self.twilioController != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_INVALID_ACTION messageAsString:@"already_started"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        });
        return;
    }
    
    NSString* token = command.arguments[0];
    NSString* room = command.arguments[1];
    NSString* avatar = command.arguments[2];
    NSString* options = command.arguments[3];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TwilioVideoIOS" bundle:nil];
        UINavigationController* nav = [sb instantiateViewControllerWithIdentifier:@"TwilioClientController"];
        self.twilioController = (TwilioClientController*)nav.topViewController;
        self.twilioController.accessToken = token;
        self.twilioController.options = options;
        self.twilioController.delegate = self;
        self.twilioController.isHost = NO;

        [self.viewController presentViewController:nav animated:YES completion:^{
            [self.twilioController setupAvatar:[NSURL URLWithString:avatar]];
            [self.twilioController setupOptions];
            [self.twilioController connectToRoom:room];
            
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ok"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    });

}

- (void)openRoomForHost:(CDVInvokedUrlCommand*)command {
    
    if (self.twilioController != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_INVALID_ACTION messageAsString:@"already_started"];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        });
        return;
    }

    NSString* token = command.arguments[0];
    NSString* room = command.arguments[1];
    NSString* options = command.arguments[2];

    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"TwilioVideoIOS" bundle:nil];
        self.containerController = [sb instantiateViewControllerWithIdentifier:@"TwilioClientController"];
        self.twilioController = (TwilioClientController*)self.containerController.topViewController;
        self.twilioController.accessToken = token;
        self.twilioController.options = options;
        self.twilioController.delegate = self;
        self.twilioController.isHost = YES;
        [self.twilioController connectToRoom:room];
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ok"];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    });
    
}

- (void) closeRoom:(CDVInvokedUrlCommand*)command {
    dispatch_async(dispatch_get_main_queue(), ^{
        CDVPluginResult* pluginResult;
        if (self.twilioController) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"ok"];
            [self.twilioController disconnect];
            self.twilioController = nil;
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_NO_RESULT messageAsString:@"room_not_opened"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    });
}

- (void) waitStatus:(CDVInvokedUrlCommand*)command {
    NSLog(@"=========== START waitStatus");
    self.waitStatusCommand = command;
}

- (void)callStatusChanged:(NSString*)status {
    __block NSString* result = status;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.waitStatusCommand) {
            NSLog(@"callStatusChanged: %@", status);
            if (self.twilioController.isHost && [result isEqualToString:@"PARTICIPANT_CONNECTED"]) {
                [self.viewController presentViewController:self.containerController animated:YES completion:^{
                    [self.twilioController setupOptions];
                }];
            }
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:result];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.waitStatusCommand.callbackId];
            self.waitStatusCommand = nil;
            if ([status isEqualToString:@"HANGED_UP"] ||
                [status isEqualToString:@"VERIFICATION_PASSED"] ||
                [status isEqualToString:@"VERIFICATION_DECLINED"] ||
                [status isEqualToString:@"MANUAL_CLOSE"])
            {
                self.twilioController = nil;
            }
            if ([status isEqualToString:@"PARTICIPANT_DISCONNECTED"] && !self.twilioController.isHost) {
                self.twilioController = nil;
            }
        }
    });
}

- (void)callStatusChanged:(NSString*)status withError:(NSString*)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.waitStatusCommand) {
            NSArray* message = [NSArray arrayWithObjects:status, error, nil];
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsMultipart:message];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.waitStatusCommand.callbackId];
            self.waitStatusCommand = nil;
            if ([status isEqualToString:@"CONNECTION_LOST"]) {
                self.twilioController = nil;
            }
        }
    });
}

@end
