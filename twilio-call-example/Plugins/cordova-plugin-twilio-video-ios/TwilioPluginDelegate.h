//
//  TwilioPluginDelegate.h
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import <Foundation/Foundation.h>

@protocol TwilioPluginDelegate <NSObject>

- (void)callStatusChanged:(NSString*)status;
- (void)callStatusChanged:(NSString*)status withError:(NSString*)error;

@end
