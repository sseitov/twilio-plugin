//
//  GuiService.h
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import <Foundation/Foundation.h>

BOOL IS_PAD(void);
BOOL IS_PORTRAIT(void);
NSString* stringForTime(NSString* prefix, int time);
CGSize screenScale(void);
