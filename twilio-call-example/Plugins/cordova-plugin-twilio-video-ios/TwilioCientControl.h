//
//  TwilioCientControl.h
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import <Foundation/Foundation.h>

@protocol TwilioCientControl <NSObject>

- (void)setupDelegate:(id)delegate;
- (void)setupUI:(NSDictionary*)options;

@optional
- (void)setupAvatar:(NSURL*)avatarURL;
- (void)connectedToRoom;
- (void)startCall;
- (void)stopCall;

@end
