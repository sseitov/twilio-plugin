//
//  GuiService.m
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import "GuiService.h"


BOOL IS_PAD() {
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}

BOOL IS_PORTRAIT() {
    return UIScreen.mainScreen.bounds.size.width < UIScreen.mainScreen.bounds.size.height;
}

NSString* stringForTime(NSString* prefix, int time) {
    if (IS_PAD()) {
        return [NSString stringWithFormat:@"%@ 00:%.2d:%.2d", prefix, time/60, time % 60];
    } else {
        return [NSString stringWithFormat:@"%.2d:%.2d", time/60, time % 60];
    }
}

CGSize screenScale() {
    
    CGFloat screenWidth = MIN(UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
    CGFloat screenHeight = MAX(UIScreen.mainScreen.bounds.size.width, UIScreen.mainScreen.bounds.size.height);
    CGFloat maketScreenWidth = 256;
    CGFloat maketScreenHeight = 324;
    return CGSizeMake(screenWidth / maketScreenWidth, screenHeight / maketScreenHeight);
}
