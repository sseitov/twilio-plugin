//
//  TwilioControlDelegate.h
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import <Foundation/Foundation.h>

@protocol TwilioControlDelegate <NSObject>

- (void)hangUp;
- (void)completeVerification;
- (void)retryVerificationLater;

@end
