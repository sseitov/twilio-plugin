//
//  TwilioClientController.m
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import "TwilioClientController.h"
#import "TwilioControlDelegate.h"
#import "TwilioCientControl.h"
#import "GuiService.h"
#import <TwilioVideo/TwilioVideo.h>

@interface TwilioClientController () <TVIRoomDelegate, TVIParticipantDelegate, TwilioControlDelegate, TVIVideoViewDelegate> {
    int callTime;
    BOOL inCall;
}

#pragma mark Video SDK components

@property (nonatomic, strong) TVIRoom *room;
@property (nonatomic, strong) TVILocalVideoTrack *localVideoTrack;
@property (nonatomic, strong) TVILocalAudioTrack *localAudioTrack;
@property (nonatomic, strong) TVIParticipant *participant;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remoteViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remoteViewHeight;

#pragma mark UI Element Outlets and handles

@property (weak, nonatomic) IBOutlet UIView *hostControlView;
@property (weak, nonatomic) IBOutlet UIView *guestControlView;

@property (weak, nonatomic) IBOutlet TVIVideoView *remoteView;
@property (weak, nonatomic) IBOutlet TVIVideoView *previewView;

@property (weak, nonatomic) id<TwilioCientControl> control;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) NSTimer *callTimer;
@property (nonatomic, strong) NSString *finishStatus;
@property (nonatomic, strong) NSString *callDurationText;
@property (nonatomic, strong) UILabel *participantName;

@end

@implementation TwilioClientController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isHost) {
        self.guestControlView.hidden = YES;
    } else {
        self.hostControlView.hidden = YES;
        [self.navigationController setNavigationBarHidden:YES];
    }
    self.previewView.alpha = 0;
    self.remoteView.delegate = self;
}

#pragma mark - Preview layout

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self resizePreview];
}

- (void)resizePreview {
    
    int maxWidth = self.localVideoTrack.constraints.maxSize.width;
    int maxHeight= self.localVideoTrack.constraints.maxSize.height;
    
    if (maxWidth == 0 || maxHeight == 0) {
        _previewView.frame = CGRectMake(0, 0, 0, 0);
        return;
    }
    
    CGFloat max = MAX(maxWidth, maxHeight);
    CGFloat min = MIN(maxWidth, maxHeight);
    CGFloat previewAspect = max/min;
    
    CGFloat maketPreviewSize = 50;
    CGFloat previewWidth = 0;
    CGFloat previewHeight = 0;
    CGFloat previewSize = maketPreviewSize * screenScale().width;
    
    if (IS_PORTRAIT()) {
        previewWidth = previewSize;
        previewHeight = previewSize * previewAspect;
    } else {
        previewHeight = previewSize;
        previewWidth = previewSize * previewAspect;
    }
    
    CAShapeLayer *aCircle = [CAShapeLayer layer];
    UIBezierPath* path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, previewSize, previewSize)];
    aCircle.path = path.CGPath;
    
    if (IS_PORTRAIT()) {
        aCircle.position = CGPointMake(0,  (previewHeight - previewSize)/2);
    } else {
        aCircle.position = CGPointMake((previewWidth - previewSize)/2, 0);
    }
    
    _previewView.layer.mask = aCircle;
    _previewView.clipsToBounds = true;
    
    CGPoint pos = [self previewPosition:CGSizeMake(previewWidth, previewHeight) size:previewSize];
    _previewView.frame = CGRectMake(pos.x, pos.y, previewWidth, previewHeight);
    [self.view setNeedsDisplay];
}

- (CGPoint)previewPosition:(CGSize)previewSize size:(CGFloat)size {
    
    CGFloat maketPreviewRightPadding = 10;
    CGFloat previewRightPadding = maketPreviewRightPadding * screenScale().width;
    CGFloat maketPreviewBottomPadding = 20;
    CGFloat previewBottomPadding = maketPreviewBottomPadding * screenScale().height;
    
    CGFloat rightPadding;
    CGFloat bottomPadding;
    
    if (IS_PORTRAIT()) {
        rightPadding = previewRightPadding;
        bottomPadding = previewBottomPadding - (previewSize.height - size)/2;
    } else {
        bottomPadding = previewBottomPadding;
        rightPadding = previewRightPadding - (previewSize.width - size)/2;
    }
    
    if (self.isHost) {
        CGFloat x = round(rightPadding);
        CGFloat offset = 44;
        if (!IS_PORTRAIT() && !IS_PAD()) {
            offset = 30;
        }
        CGFloat y = round(bottomPadding) + offset;
        return CGPointMake(x, y);
    } else {
        CGFloat x = self.view.frame.size.width - round(rightPadding) - previewSize.width;
        CGFloat y = self.view.frame.size.height - round(bottomPadding) - previewSize.height;
        return CGPointMake(x, y);
    }
}

#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (self.isHost) {
        return [identifier isEqualToString:@"host"];
    } else {
        return [identifier isEqualToString:@"guest"];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.control = segue.destinationViewController;
    [self.control setupDelegate:self];
}

#pragma mark - TwilioControlDelegate

- (void)hangUp {
    self.finishStatus = @"HANGED_UP";
    [self.room disconnect];
}

- (void)completeVerification {
    self.finishStatus = @"VERIFICATION_PASSED";
    [self.room disconnect];
}

- (void)retryVerificationLater {
    self.finishStatus = @"VERIFICATION_DECLINED";
    [self.room disconnect];
}

#pragma mark - Public plugin methods

- (void)connectToRoom:(NSString*)room {
    if (!self.accessToken) {
        NSLog(@"Please provide a valid token to connect to a room");
        return;
    }
    
    self.localAudioTrack = [TVILocalAudioTrack track];
    TVICameraCapturer* camera = [[TVICameraCapturer alloc] initWithSource:TVICameraCaptureSourceFrontCamera];
    self.localVideoTrack = [TVILocalVideoTrack trackWithCapturer:camera];

    TVIConnectOptions *connectOptions = [TVIConnectOptions optionsWithToken:self.accessToken
                                                                      block:^(TVIConnectOptionsBuilder * _Nonnull builder) {
                                                                          // Use the local media that we prepared earlier.
                                                                          builder.audioTracks = @[self.localAudioTrack];
                                                                          builder.videoTracks = @[self.localVideoTrack];
                                                                          builder.roomName = room;
                                                                      }];

    // Connect to the Room using the options we provided.
    self.room = [TwilioVideo connectWithOptions:connectOptions delegate:self];
    NSLog(@"%@", [NSString stringWithFormat:@"Attempting to connect to room %@", room]);
    [self.view layoutSubviews];
}

- (void)disconnect {
    self.finishStatus = @"MANUAL_CLOSE";
    [self.room disconnect];
}

- (void)setupAvatar:(NSURL*)avatarURL {
    if ([self.control respondsToSelector:@selector(setupAvatar:)]) {
        [self.control setupAvatar:avatarURL];
    }
}

- (void)setupOptions {
    NSError* err = NULL;
    NSData* data = [self.options dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
    NSLog(@"%@", json);
    if (json && !err) {
        _callDurationText = [json objectForKey:@"callDurationText"];
        [self.control setupUI:json];
    }
}

#pragma mark - Call management

- (void)cleanupRemoteParticipant {
    
    if (self.participant) {
        if ([self.participant.videoTracks count] > 0) {
            [self.participant.videoTracks[0] removeRenderer:self.remoteView];
        }
        self.participant = nil;
    }
}

- (void)startCall:(NSString*)participant {
    
    if (inCall) {
        return;
    }
    inCall = YES;
    [UIView animateWithDuration:0.4 animations:^() {
        self->_previewView.alpha = 1;
    }];
    if (!self.isHost) {
        [self.control startCall];
    }
    
    _participantName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    _participantName.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _participantName.textAlignment = NSTextAlignmentCenter;
    _participantName.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
    _participantName.numberOfLines = 0;
    _participantName.lineBreakMode = NSLineBreakByWordWrapping;
    _participantName.text = participant;
    self.navigationItem.titleView = _participantName;

    callTime = 0;
    if (IS_PAD()) {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    } else {
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 44)];
    }
    _timeLabel.text = stringForTime(_callDurationText, callTime);
    _timeLabel.textAlignment = NSTextAlignmentRight;
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithCustomView:_timeLabel];
    [self.navigationItem setRightBarButtonItem:btn animated:YES];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];

    _callTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(onTick:) userInfo:nil repeats:YES];
    
    [self.delegate callStatusChanged:@"PARTICIPANT_CONNECTED"];
}

- (void)stopCall {
    
    if (!inCall) {
        return;
    }
    inCall = NO;
    
    if (!self.isHost) {
        self.finishStatus = @"PARTICIPANT_DISCONNECTED";
        [self.room disconnect];
    } else {
        [UIView animateWithDuration:0.4 animations:^() {
            self->_previewView.alpha = 0;
        }];
        [_callTimer invalidate];
        [self.navigationItem setRightBarButtonItem:nil animated:YES];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        [self.delegate callStatusChanged:@"PARTICIPANT_DISCONNECTED"];
    }
}

-(void)onTick:(NSTimer*)timer {
    callTime++;
    _timeLabel.text = stringForTime(_callDurationText, callTime);
}

#pragma mark - TVIRoomDelegate

- (void)didConnectToRoom:(nonnull TVIRoom *)room {
    
    NSLog(@"%@",[NSString stringWithFormat:@"Connected to room %@ as %@", room.name, room.localParticipant.identity]);
    
    if (room.participants.count > 0) {
        self.participant = room.participants[0];
        self.participant.delegate = self;
    }
    [self.delegate callStatusChanged:@"CONNECTED_TO_ROOM"];

    if (!self.isHost) {
        [self.control connectedToRoom];
    }
 
    if (!self.localVideoTrack) {
        NSLog(@"Failed to add video track");
    } else {
        // Attach view to video track for local preview
        [self.localVideoTrack addRenderer:self.previewView];
        NSLog(@"Video track added to localMedia");
        [self.view setNeedsLayout];
    }

}


- (void)room:(TVIRoom *)room didFailToConnectWithError:(nonnull NSError *)error {
    
    NSString* errorMsg = [NSString stringWithFormat:@"Failed to connect to room, error = %@", error];
    NSLog(@"%@", errorMsg);
    if (self.delegate) {
        [self.delegate callStatusChanged:@"CONNECTION_LOST" withError:errorMsg];
    }
    self.room = nil;
    if (!self.isHost) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)room:(nonnull TVIRoom *)room didDisconnectWithError:(nullable NSError *)error {
    
    [self cleanupRemoteParticipant];
    self.room = nil;
    
    if (error != nil) {
        NSString* errorMsg = [NSString stringWithFormat:@"Disconncted from room %@, error = %@", room.name, error];
        NSLog(@"%@", errorMsg);
        if (self.delegate) {
            [self.delegate callStatusChanged:@"CONNECTION_LOST" withError:errorMsg];
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.delegate callStatusChanged: self.finishStatus];
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}

- (void)room:(TVIRoom *)room participantDidConnect:(TVIParticipant *)participant {
    
    if (!self.participant) {
        self.participant = participant;
        self.participant.delegate = self;
    }
    NSLog(@"%@", [NSString stringWithFormat:@"Room %@ participant %@ connected", room.name, participant.identity]);
    [self startCall:participant.identity];
}

- (void)room:(TVIRoom *)room participantDidDisconnect:(TVIParticipant *)participant {
    
    if (self.participant == participant) {
        [self cleanupRemoteParticipant];
    }
    [self stopCall];
    NSLog(@"%@", [NSString stringWithFormat:@"Room %@ participant %@ disconnected", room.name, participant.identity]);
}

#pragma mark - TVIParticipantDelegate

- (void)participant:(TVIParticipant *)participant addedVideoTrack:(TVIVideoTrack *)videoTrack {
    
    NSLog(@"%@", [NSString stringWithFormat:@"Participant %@ added video track.", participant.identity]);
    
    if (self.participant == participant) {
        [videoTrack addRenderer:self.remoteView];
    }
    [self startCall:participant.identity];
}

- (void)participant:(TVIParticipant *)participant removedVideoTrack:(TVIVideoTrack *)videoTrack {
    
    NSLog(@"%@", [NSString stringWithFormat:@"Participant %@ removed video track.", participant.identity]);
    
    if (self.participant == participant) {
        [videoTrack removeRenderer:self.remoteView];
    }
    [self stopCall];
}

- (void)participant:(TVIParticipant *)participant addedAudioTrack:(TVIAudioTrack *)audioTrack {
    
    NSLog(@"%@", [NSString stringWithFormat:@"Participant %@ added audio track.", participant.identity]);
}

- (void)participant:(TVIParticipant *)participant removedAudioTrack:(TVIAudioTrack *)audioTrack {
    
    NSLog(@"%@", [NSString stringWithFormat:@"Participant %@ removed audio track.", participant.identity]);
}

- (void)participant:(TVIParticipant *)participant enabledTrack:(TVITrack *)track {
    
    NSString *type = @"";
    if ([track isKindOfClass:[TVIAudioTrack class]]) {
        type = @"audio";
    } else {
        type = @"video";
    }
    NSLog(@"%@", [NSString stringWithFormat:@"Participant %@ enabled %@ track.", participant.identity, type]);
}

- (void)participant:(TVIParticipant *)participant disabledTrack:(TVITrack *)track {
    
    NSString *type = @"";
    if ([track isKindOfClass:[TVIAudioTrack class]]) {
        type = @"audio";
    } else {
        type = @"video";
    }
    NSLog(@"%@", [NSString stringWithFormat:@"Participant %@ disabled %@ track.", participant.identity, type]);
}

#pragma mark - TVIVideoViewDelegate

- (void)videoView:(TVIVideoView *)view videoDimensionsDidChange:(CMVideoDimensions)dimensions {
    NSLog(@"====== videoDimensionsDidChange (is remoteView(%d)) width %d, height %d", (view == self.remoteView), dimensions.width, dimensions.height);
    _remoteViewWidth.constant = dimensions.width;
    _remoteViewHeight.constant = dimensions.height;
    [self.view setNeedsLayout];
}

- (void)videoView:(TVIVideoView *)view videoOrientationDidChange:(TVIVideoOrientation)orientation {
    NSLog(@"!!!!!!!!!!!! videoOrientationDidChange %lu", orientation);
    [self.view setNeedsLayout];
}

@end
