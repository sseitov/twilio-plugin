//
//  TwilioGuestControlController.m
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import "TwilioGuestControlController.h"
#import "TwilioControlDelegate.h"
#import "GuiService.h"

@interface TwilioGuestControlController ()

@property (weak, nonatomic) id<TwilioControlDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIButton *hangupButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hangupHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hangupWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hangupBottomConstraint;

@property (weak, nonatomic) IBOutlet UIView *waitingView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *waitingLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *waitingHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *waitingWidthConstraint;

@end

@implementation TwilioGuestControlController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hangupButton.alpha = 0;
    _waitingView.alpha = 1;
    
    _avatarView.clipsToBounds = true;
    if (!IS_PAD()) {
        _waitingLabel.font = [UIFont systemFontOfSize:12];
    }

    CGFloat maketLogoSize = 80;
    CGFloat logoSize = maketLogoSize * screenScale().width;
    _waitingWidthConstraint.constant = round(logoSize);
    _waitingHeightConstraint.constant = round(logoSize) + 60;
  
    CGFloat maketButtonSize = 40;
    CGFloat buttonSize = maketButtonSize * screenScale().width;
    CGFloat maketButtonBottomPadding = 20;
    CGFloat buttonBottomPadding = maketButtonBottomPadding * screenScale().height;
    
    _hangupHeightConstraint.constant = round(buttonSize);
    _hangupWidthConstraint.constant = round(buttonSize);
    _hangupBottomConstraint.constant =  round(buttonBottomPadding);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _avatarView.layer.cornerRadius = _avatarView.bounds.size.width / 2;
}

- (IBAction)doHangUp:(id)sender {
    [self.delegate hangUp];
}

#pragma mark - TwilioCientControl

- (void)setupAvatar:(NSURL*)avatarURL {

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        NSData* avatarData = [NSData dataWithContentsOfURL:avatarURL];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (avatarData) {
                self->_avatarView.image = [UIImage imageWithData:avatarData];
            }
        });
    });
}

- (void)setupUI:(NSDictionary*)options {
    NSString* hangUpButton = [options objectForKey:@"hangUpButton"];
    NSURL* url = [NSURL URLWithString:hangUpButton];
    if (url) {
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData* imageData = [NSData dataWithContentsOfURL:url];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (imageData) {
                    UIImage* image = [UIImage imageWithData:imageData];
                    [self.hangupButton setImage:image forState:UIControlStateNormal];
                }
            });
        });
    }
    _waitingLabel.text = [options objectForKey:@"waitHostText"];
}

- (void)setupDelegate:(id)delegate {
    self.delegate = delegate;
}

- (void)connectedToRoom {
    [UIView animateWithDuration:0.4 animations:^() {
        self->_hangupButton.alpha = 1;
    }];
}

- (void)startCall {
    [UIView animateWithDuration:0.4 animations:^() {
        self->_waitingView.alpha = 0;
    }];
}

- (void)stopCall {
    [UIView animateWithDuration:0.4 animations:^() {
        self->_waitingView.alpha = 1;
    }];
}

@end
