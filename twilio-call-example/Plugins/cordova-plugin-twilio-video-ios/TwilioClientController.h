//
//  TwilioClientController.h
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import <UIKit/UIKit.h>
#import "TwilioPluginDelegate.h"

@interface TwilioClientController : UIViewController

@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *options;
@property (weak, nonatomic) id<TwilioPluginDelegate> delegate;
@property (nonatomic) BOOL isHost;

- (void)connectToRoom:(NSString*)room;
- (void)disconnect;
- (void)setupAvatar:(NSURL*)avatarURL;
- (void)setupOptions;

@end
