//
//  TwilioHostControlController.m
//  twilio-call-is-example
//
//  Created by Сергей Сейтов on 29.09.2017.
//

#import "TwilioHostControlController.h"
#import "TwilioControlDelegate.h"
#import "GuiService.h"

@interface TwilioHostControlController ()

@property (weak, nonatomic) id<TwilioControlDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *completeButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *completeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *completeWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *completeBottomConstraint;

@property (weak, nonatomic) IBOutlet UIButton *retryButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *retryWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *retryHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *retryBottomConstraint;

@end

@implementation TwilioHostControlController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _completeButton.clipsToBounds = true;
    _retryButton.clipsToBounds = true;
    
    if (IS_PAD()) {
        _completeButton.titleLabel.font = [UIFont systemFontOfSize:21];
        _retryButton.titleLabel.font = [UIFont systemFontOfSize:21];
    }
    
    CGFloat maketButtonWidth = 157;
    CGFloat buttonWidth = round(maketButtonWidth * screenScale().width);
    _completeWidthConstraint.constant = buttonWidth;
    _retryWidthConstraint.constant = buttonWidth;
    
    CGFloat maketButtonHeight = 20;
    CGFloat buttonHeight = round(maketButtonHeight * screenScale().height);
    _completeHeightConstraint.constant = buttonHeight;
    _retryHeightConstraint.constant = buttonHeight;
    
    CGFloat maketButtonBottomPadding = 20;
    _retryBottomConstraint.constant = round(maketButtonBottomPadding * screenScale().height);
    
    CGFloat maketButtonLeading = 6.7;
    _completeBottomConstraint.constant = round(maketButtonLeading * screenScale().height);
    
    _completeButton.layer.cornerRadius = buttonHeight / 2;
    _retryButton.layer.cornerRadius = buttonHeight / 2;
}

- (IBAction)completeVerification:(id)sender {
    [self.delegate completeVerification];
}

- (IBAction)retryVerification:(id)sender {
    [self.delegate retryVerificationLater];
}

#pragma mark - TwilioCientControl

- (void)setupDelegate:(id)delegate {
    self.delegate = delegate;
}

- (void)setupUI:(NSDictionary*)options {
    [_completeButton setTitle:[options objectForKey:@"completeVerificationButtonText"] forState:UIControlStateNormal];
    [_retryButton setTitle:[options objectForKey:@"retryVerificationButtonText"] forState:UIControlStateNormal];
}

@end
